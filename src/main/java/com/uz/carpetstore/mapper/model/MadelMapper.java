package com.uz.carpetstore.mapper.model;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.domain.model._Model;
import com.uz.carpetstore.dto.model.ModelCreateDto;
import com.uz.carpetstore.dto.model.ModelDto;
import com.uz.carpetstore.dto.model.ModelUpdateDto;
import com.uz.carpetstore.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface MadelMapper extends BaseMapper<_Model, ModelDto, ModelCreateDto, ModelUpdateDto> {
}
