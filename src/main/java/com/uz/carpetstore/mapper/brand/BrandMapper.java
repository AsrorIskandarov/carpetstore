package com.uz.carpetstore.mapper.brand;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.domain.brand._Brand;
import com.uz.carpetstore.dto.brand.BrandCreateDto;
import com.uz.carpetstore.dto.brand.BrandDto;
import com.uz.carpetstore.dto.brand.BrandUpdateDto;
import com.uz.carpetstore.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface BrandMapper extends BaseMapper<_Brand, BrandDto, BrandCreateDto, BrandUpdateDto> {


}
