package com.uz.carpetstore.mapper.history;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.domain.history._History;
import com.uz.carpetstore.dto.history.HistoryCreateDto;
import com.uz.carpetstore.dto.history.HistoryDto;
import com.uz.carpetstore.dto.history.HistoryUpdateDto;
import com.uz.carpetstore.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface HistoryMapper extends BaseMapper<_History, HistoryDto, HistoryCreateDto, HistoryUpdateDto> {



}
