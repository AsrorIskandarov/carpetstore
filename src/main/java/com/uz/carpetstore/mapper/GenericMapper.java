package com.uz.carpetstore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.domain.Auditable;
import com.uz.carpetstore.dto.GenericDto;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface GenericMapper {
    GenericDto fromDomain(Auditable domain);
}
