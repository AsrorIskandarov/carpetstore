package com.uz.carpetstore.mapper.auth;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.domain.auth._Permission;
import com.uz.carpetstore.dto.auth.PermissionCreateDto;
import com.uz.carpetstore.dto.auth.PermissionDto;
import com.uz.carpetstore.dto.auth.PermissionUpdateDto;
import com.uz.carpetstore.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface PermissionMapper extends BaseMapper<_Permission, PermissionDto, PermissionCreateDto, PermissionUpdateDto> {
}
