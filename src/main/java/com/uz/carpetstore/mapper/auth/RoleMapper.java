package com.uz.carpetstore.mapper.auth;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.domain.auth._Role;
import com.uz.carpetstore.dto.auth.RoleCreateDto;
import com.uz.carpetstore.dto.auth.RoleDto;
import com.uz.carpetstore.dto.auth.RoleUpdateDto;
import com.uz.carpetstore.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = {PermissionMapper.class})
@Component
public interface RoleMapper extends BaseMapper<_Role, RoleDto, RoleCreateDto, RoleUpdateDto> {

}
