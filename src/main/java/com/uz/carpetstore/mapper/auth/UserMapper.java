package com.uz.carpetstore.mapper.auth;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.domain.auth._User;
import com.uz.carpetstore.dto.auth.UserCreateDto;
import com.uz.carpetstore.dto.auth.UserDto;
import com.uz.carpetstore.dto.auth.UserMeDto;
import com.uz.carpetstore.dto.auth.UserUpdateDto;
import com.uz.carpetstore.dto.signUp.UserSignUpDto;
import com.uz.carpetstore.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", uses = {RoleMapper.class})
@Component
public interface UserMapper extends BaseMapper<_User, UserDto, UserCreateDto, UserUpdateDto> {

    @Override
    @Mapping(source = "userType.code", target = "userTypeName")
    UserDto toDto(_User entity);

    @Mapping(source = "userType.code", target = "userTypeName")
    UserMeDto toUserMeDto(_User entity);

    _User fromDto(UserSignUpDto dto);
}
