package com.uz.carpetstore.mapper.category;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.domain.category._Category;
import com.uz.carpetstore.dto.category.CategoryCreateDto;
import com.uz.carpetstore.dto.category.CategoryDto;
import com.uz.carpetstore.dto.category.CategoryUpdateDto;
import com.uz.carpetstore.mapper.BaseMapper;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
@Component
public interface CategoryMapper extends BaseMapper<_Category, CategoryDto, CategoryCreateDto, CategoryUpdateDto> {
}
