package com.uz.carpetstore.domain.category;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import com.uz.carpetstore.domain.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "categories")
public class _Category extends Auditable implements GrantedAuthority {
    @Column(unique = true)
    private String name;
    @Override
    public String getAuthority() {return name;}
}
