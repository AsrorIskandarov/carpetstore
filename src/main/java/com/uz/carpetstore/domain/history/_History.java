package com.uz.carpetstore.domain.history;

import lombok.*;
import lombok.experimental.FieldDefaults;
import com.uz.carpetstore.domain.Auditable;
import com.uz.carpetstore.domain.model._Model;
import com.uz.carpetstore.enums.IncomeOutcome;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static lombok.AccessLevel.PRIVATE;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
@Table(name = "history")
public class _History extends Auditable {

    IncomeOutcome status;

    Integer count;

    Double price;

    @ManyToOne
    _Model model;

}
