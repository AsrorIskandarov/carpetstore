package com.uz.carpetstore.domain.setting;

import lombok.*;
import com.uz.carpetstore.domain.Auditable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "languages")
public class _Language extends Auditable {

    @Column(name = "name")
    protected String name;

    @Column(name = "code")
    protected String code;
}
