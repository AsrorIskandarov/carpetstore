package com.uz.carpetstore.domain.model;

import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import com.uz.carpetstore.domain.Auditable;
import com.uz.carpetstore.domain.brand._Brand;
import com.uz.carpetstore.domain.category._Category;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "models")
public class _Model extends Auditable implements GrantedAuthority {
    @Column(unique = true)
    private String name;

    @Column
    private Integer count;

    @Column
    private Double price;

    @ManyToOne
    _Brand brand;

    @ManyToOne
    _Category category;
    @Override
    public String getAuthority() {
        return getName();
    }
}
