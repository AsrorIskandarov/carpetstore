package com.uz.carpetstore.repository.auth;

import com.uz.carpetstore.criteria.auth.UserCriteria;
import com.uz.carpetstore.domain.auth._User;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface IUserRepository extends IGenericCrudRepository<_User, UserCriteria> {

    _User findByUsername(String username);
    _User findByEmail(String email);

}
