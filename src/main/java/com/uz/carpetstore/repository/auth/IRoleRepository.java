package com.uz.carpetstore.repository.auth;

import com.uz.carpetstore.criteria.auth.RoleCriteria;
import com.uz.carpetstore.domain.auth._Role;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface IRoleRepository extends IGenericCrudRepository<_Role, RoleCriteria> {
}
