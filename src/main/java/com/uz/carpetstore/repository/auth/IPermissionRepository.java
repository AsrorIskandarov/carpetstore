package com.uz.carpetstore.repository.auth;

import com.uz.carpetstore.criteria.auth.PermissionCriteria;
import com.uz.carpetstore.domain.auth._Permission;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface IPermissionRepository extends IGenericCrudRepository<_Permission, PermissionCriteria> {
}
