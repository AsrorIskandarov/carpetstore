package com.uz.carpetstore.repository;

import com.uz.carpetstore.criteria.GenericCriteria;
import com.uz.carpetstore.domain.Auditable;

public interface IGenericCrudRepository<T extends Auditable, C extends GenericCriteria> extends IGenericRepository<T, C> {

    Long save(T entity);

    T update(T entity);

    void delete(T entity);
}
