package com.uz.carpetstore.repository.brand;

import com.uz.carpetstore.criteria.brand.BrandCriteria;
import com.uz.carpetstore.domain.brand._Brand;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface IBrandRepository  extends IGenericCrudRepository<_Brand, BrandCriteria> {
}
