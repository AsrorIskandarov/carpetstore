package com.uz.carpetstore.repository.brand.impl;

import org.springframework.stereotype.Repository;
import com.uz.carpetstore.criteria.brand.BrandCriteria;
import com.uz.carpetstore.domain.brand._Brand;
import com.uz.carpetstore.repository.GenericDao;
import com.uz.carpetstore.repository.brand.IBrandRepository;

import java.util.List;
import java.util.Map;

@Repository
public class BrandRepository extends GenericDao<_Brand, BrandCriteria>
        implements IBrandRepository {
    @Override
    protected void defineCriteriaOnQuerying(
            BrandCriteria criteria,
            List<String> whereCause,
            Map<String, Object> params,
            StringBuilder queryBuilder) {

        if (!utils.isEmpty(criteria.getName())) {
            boolean add = whereCause.add(" t.name like %:name% ");
            params.put("name", criteria.getName());
        }

        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add(" t.id = :selfId ");
            params.put("selfId", criteria.getSelfId());
        }

        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
}
