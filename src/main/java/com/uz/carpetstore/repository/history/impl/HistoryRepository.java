package com.uz.carpetstore.repository.history.impl;

import org.springframework.stereotype.Repository;
import com.uz.carpetstore.criteria.history.HistoryCriteria;
import com.uz.carpetstore.domain.history._History;
import com.uz.carpetstore.repository.GenericDao;
import com.uz.carpetstore.repository.history.IHistoryRepository;

import java.util.List;
import java.util.Map;

@Repository
public class HistoryRepository extends GenericDao<_History, HistoryCriteria> implements IHistoryRepository {

    @Override
    protected void defineCriteriaOnQuerying(
            HistoryCriteria criteria,
            List<String> whereCause,
            Map<String, Object> params,
            StringBuilder queryBuilder) {

        if (!utils.isEmpty(criteria.getStatus())) {
            whereCause.add(" t.text = :text");
            params.put("text", criteria.getStatus());
        }


        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add(" t.id = :selfId ");
            params.put("selfId", criteria.getSelfId());
        }

        if (!utils.isEmpty(criteria.getCount())) {
            whereCause.add(" t.count like = :count ");
            params.put("count", criteria.getCount());
        }

        if (!utils.isEmpty(criteria.getPrice())) {
            whereCause.add(" t.price like %:price% ");
            params.put("price", criteria.getPrice());
        }

        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }

}
