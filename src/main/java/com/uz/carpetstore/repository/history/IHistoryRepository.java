package com.uz.carpetstore.repository.history;

import com.uz.carpetstore.criteria.history.HistoryCriteria;
import com.uz.carpetstore.domain.history._History;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface IHistoryRepository extends IGenericCrudRepository<_History, HistoryCriteria> {
}
