package com.uz.carpetstore.repository.setting;

import com.uz.carpetstore.criteria.setting.LanguageCriteria;
import com.uz.carpetstore.domain.setting._Language;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface ILanguageRepository extends IGenericCrudRepository<_Language, LanguageCriteria> {

}
