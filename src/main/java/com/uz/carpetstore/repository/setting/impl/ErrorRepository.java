package com.uz.carpetstore.repository.setting.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.uz.carpetstore.criteria.GenericCriteria;
import com.uz.carpetstore.domain.Auditable;
import com.uz.carpetstore.domain.setting._ErrorMessage;
import com.uz.carpetstore.enums.ErrorCodes;
import com.uz.carpetstore.repository.GenericDao;
import com.uz.carpetstore.repository.setting.IErrorRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

@Repository
public class ErrorRepository extends GenericDao<Auditable, GenericCriteria> implements IErrorRepository {

    /**
     * Common logger for use in subclasses.
     */
    protected final Log logger = LogFactory.getLog(getClass());

    @Autowired
    protected EntityManager entityManager;

    @Override
    public String getErrorMessage(ErrorCodes errorCode, String params) {
        String lang = userSession.getLanguage() == null ? "ru" : userSession.getLanguage();
        try {
            String error = (String) entityManager.createQuery("SELECT emt.name FROM _ErrorMessage t" +
                    " LEFT JOIN _ErrorMessageTranslation emt on t.id = emt.messageId " +
                    " WHERE t.errorCode = '" + errorCode + "' and emt.language.code = '" + lang + "' ORDER BY t.id DESC ").getSingleResult();
            return String.format(error, (Object[]) params.split("#"));
        } catch (NoResultException ex) {
            logger.error(ex.getMessage());
            throw new RuntimeException(String.format("Error Message with errorCode '%s' not found", errorCode));
        }
    }

    @Override
    public _ErrorMessage getByErrorCode(String errorCode) {
        try {
            return (_ErrorMessage) entityManager.createQuery("SELECT t FROM _ErrorMessage t WHERE t.errorCode = '" + errorCode + "' ORDER BY t.id DESC ").getSingleResult();
        } catch (NoResultException ex) {
            logger.error(ex.getMessage());
            throw new RuntimeException(String.format("Error Message with errorCode '%s' not found", errorCode));
        }
    }
}
