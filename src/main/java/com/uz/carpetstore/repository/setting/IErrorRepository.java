package com.uz.carpetstore.repository.setting;

import com.uz.carpetstore.criteria.GenericCriteria;
import com.uz.carpetstore.domain.Auditable;
import com.uz.carpetstore.domain.setting._ErrorMessage;
import com.uz.carpetstore.enums.ErrorCodes;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface IErrorRepository extends IGenericCrudRepository<Auditable, GenericCriteria> {

    String getErrorMessage(ErrorCodes errorCode, String params);

    _ErrorMessage getByErrorCode(String errorCode);
}
