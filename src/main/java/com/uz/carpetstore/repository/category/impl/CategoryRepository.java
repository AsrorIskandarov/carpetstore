package com.uz.carpetstore.repository.category.impl;

import org.springframework.stereotype.Repository;
import com.uz.carpetstore.criteria.category.CategoryCriteria;
import com.uz.carpetstore.domain.category._Category;
import com.uz.carpetstore.repository.GenericDao;
import com.uz.carpetstore.repository.category.ICategoryRepository;

import java.util.List;
import java.util.Map;

@Repository
public class CategoryRepository  extends GenericDao<_Category, CategoryCriteria>
        implements ICategoryRepository {
    @Override
    protected void defineCriteriaOnQuerying(
            CategoryCriteria criteria,
            List<String> whereCause,
            Map<String, Object> params,
            StringBuilder queryBuilder) {

        if (!utils.isEmpty(criteria.getName())) {
            boolean add = whereCause.add(" t.name like %:name% ");
            params.put("name", criteria.getName());
        }

        if (!utils.isEmpty(criteria.getSelfId())) {
            whereCause.add(" t.id = :selfId ");
            params.put("selfId", criteria.getSelfId());
        }

        onDefineWhereCause(criteria, whereCause, params, queryBuilder);
    }
}
