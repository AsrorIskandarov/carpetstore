package com.uz.carpetstore.repository.category;

import com.uz.carpetstore.criteria.category.CategoryCriteria;
import com.uz.carpetstore.domain.category._Category;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface ICategoryRepository extends IGenericCrudRepository<_Category, CategoryCriteria> {
}
