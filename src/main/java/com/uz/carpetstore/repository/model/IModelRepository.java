package com.uz.carpetstore.repository.model;

import com.uz.carpetstore.criteria.model.ModelCriteria;
import com.uz.carpetstore.domain.model._Model;
import com.uz.carpetstore.repository.IGenericCrudRepository;

public interface IModelRepository  extends IGenericCrudRepository<_Model, ModelCriteria> {
}
