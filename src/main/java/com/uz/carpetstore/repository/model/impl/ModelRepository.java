package com.uz.carpetstore.repository.model.impl;

import org.springframework.stereotype.Repository;
import com.uz.carpetstore.criteria.model.ModelCriteria;
import com.uz.carpetstore.domain.model._Model;
import com.uz.carpetstore.repository.GenericDao;
import com.uz.carpetstore.repository.model.IModelRepository;

import java.util.List;
import java.util.Map;

@Repository
public class ModelRepository extends GenericDao<_Model, ModelCriteria>
    implements IModelRepository {

  @Override
  protected void defineCriteriaOnQuerying(
      ModelCriteria criteria,
      List<String> whereCause,
      Map<String, Object> params,
      StringBuilder queryBuilder) {

    if (!utils.isEmpty(criteria.getName())) {
      whereCause.add(" t.name like %:name% ");
      params.put("name", criteria.getName());
    }

    if (!utils.isEmpty(criteria.getSelfId())) {
      whereCause.add(" t.id = :selfId ");
      params.put("selfId", criteria.getSelfId());
    }

    if (!utils.isEmpty(criteria.getCount())) {
      whereCause.add(" t.count like %:count% ");
      params.put("count", criteria.getCount());
    }

    if (!utils.isEmpty(criteria.getPrice())) {
      whereCause.add(" t.price like %:price% ");
      params.put("price", criteria.getPrice());
    }

    onDefineWhereCause(criteria, whereCause, params, queryBuilder);
  }
}
