package com.uz.carpetstore.repository;

import com.uz.carpetstore.criteria.GenericCriteria;
import com.uz.carpetstore.domain.Auditable;

import java.util.List;

public interface IGenericRepository<T extends Auditable, C extends GenericCriteria> extends IAbstractRepository {

    T find(Long id);

    T find(C criteria);

    Long getTotalCount(C criteria);

    List<T> findAll(C criteria);
}
