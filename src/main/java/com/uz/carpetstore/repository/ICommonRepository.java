package com.uz.carpetstore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import com.uz.carpetstore.domain.Auditable;

@NoRepositoryBean
public interface ICommonRepository<T extends Auditable> extends CrudRepository<T, Long>, IAbstractRepository {

}
