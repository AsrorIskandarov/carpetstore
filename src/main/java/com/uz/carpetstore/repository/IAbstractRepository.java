package com.uz.carpetstore.repository;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IAbstractRepository {

}
