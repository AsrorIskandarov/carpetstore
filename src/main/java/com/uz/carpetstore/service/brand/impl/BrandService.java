package com.uz.carpetstore.service.brand.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.uz.carpetstore.criteria.brand.BrandCriteria;
import com.uz.carpetstore.domain.brand._Brand;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.brand.BrandCreateDto;
import com.uz.carpetstore.dto.brand.BrandDto;
import com.uz.carpetstore.dto.brand.BrandUpdateDto;
import com.uz.carpetstore.enums.ErrorCodes;
import com.uz.carpetstore.exception.IdRequiredException;
import com.uz.carpetstore.exception.ValidationException;
import com.uz.carpetstore.mapper.GenericMapper;
import com.uz.carpetstore.mapper.brand.BrandMapper;
import com.uz.carpetstore.repository.brand.IBrandRepository;
import com.uz.carpetstore.repository.setting.IErrorRepository;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.GenericCrudService;
import com.uz.carpetstore.service.brand.IBrandService;
import com.uz.carpetstore.service.model.utils.BaseUtils;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Service(value = "brandService")
public class BrandService extends GenericCrudService<_Brand, BrandDto, BrandCreateDto, BrandUpdateDto, BrandCriteria, IBrandRepository> implements IBrandService {
    /**
     * Common logger for use in subclasses.
     */
    private final Log logger = LogFactory.getLog(getClass());
    private final BrandMapper brandMapper;
    private final GenericMapper genericMapper;
    private final Validator validator;



    @Autowired
    public BrandService(IBrandRepository repository, BaseUtils utils, IErrorRepository errorRepository, BrandMapper brandMapper, GenericMapper genericMapper, Validator validator) {
        super(repository, utils, errorRepository);
        this.brandMapper = brandMapper;
        this.genericMapper = genericMapper;
        this.validator = validator;
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_READ)")
    public ResponseEntity<DataDto<BrandDto>> get(Long id) {
        _Brand brand = repository.find(BrandCriteria.childBuilder().selfId(id).build());
        validate(brand, id);
        return new ResponseEntity<>(new DataDto<>(brandMapper.toDto(brand)), HttpStatus.OK);
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_READ)")
    public ResponseEntity<DataDto<List<BrandDto>>> getAll(BrandCriteria criteria) {
        Long total = repository.getTotalCount(criteria);
        return new ResponseEntity<>(new DataDto<>(brandMapper.toDto(repository.findAll(criteria)), total), HttpStatus.OK);
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_CREATE)")
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull BrandCreateDto dto) {
        Set<ConstraintViolation<BrandCreateDto>> violations = validator.validate(dto);

        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<BrandCreateDto> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage()).append(" ");
            }
            throw new ValidationException(sb.toString());
        }

        _Brand brand = brandMapper.fromCreateDto(dto);
        baseValidation(brand);
        brand.setName(brand.getName());
        brand.setCreatedDate(brand.getCreatedDate());
        repository.save(brand);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(brand)), HttpStatus.CREATED);
    }

    @Override
    @Transactional
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_UPDATE)")
    public ResponseEntity<DataDto<BrandDto>> update(@NotNull BrandUpdateDto dto) {
        if (utils.isEmpty(dto.getId())) {
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Brand"));
        }
        _Brand brand = repository.find(dto.getId());
        validate(brand, dto.getId());

        brand = brandMapper.fromUpdateDto(dto, brand);
        baseValidation(brand);

        brand.setName(brand.getName());
        brand.setUpdatedBy(brand.getUpdatedBy());
        brand.setUpdatedDate(brand.getUpdatedDate());
        repository.save(brand);

        return get(brand.getId());
    }

    @Override
    @Transactional
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_DELETE)")
    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        _Brand brand = repository.find(id);
        validate(brand, id);
        repository.delete(brand);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull _Brand entity) {
        if (utils.isEmpty(entity.getName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("name", "Brand")));
    }

    @Override
    public void validate(@NotNull _Brand entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Brand with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Brand", id)));
        }
    }
}
