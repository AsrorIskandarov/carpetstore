package com.uz.carpetstore.service.brand;

import com.uz.carpetstore.criteria.brand.BrandCriteria;
import com.uz.carpetstore.domain.brand._Brand;
import com.uz.carpetstore.dto.brand.BrandCreateDto;
import com.uz.carpetstore.dto.brand.BrandDto;
import com.uz.carpetstore.dto.brand.BrandUpdateDto;
import com.uz.carpetstore.service.IGenericCrudService;


public interface IBrandService extends IGenericCrudService<_Brand, BrandDto, BrandCreateDto, BrandUpdateDto, Long, BrandCriteria> {
}
