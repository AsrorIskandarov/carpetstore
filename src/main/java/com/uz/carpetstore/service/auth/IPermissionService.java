package com.uz.carpetstore.service.auth;

import com.uz.carpetstore.criteria.auth.PermissionCriteria;
import com.uz.carpetstore.domain.auth._Permission;
import com.uz.carpetstore.dto.auth.PermissionCreateDto;
import com.uz.carpetstore.dto.auth.PermissionDto;
import com.uz.carpetstore.dto.auth.PermissionUpdateDto;
import com.uz.carpetstore.service.IGenericCrudService;

public interface IPermissionService extends IGenericCrudService<_Permission, PermissionDto, PermissionCreateDto, PermissionUpdateDto, Long, PermissionCriteria> {

}
