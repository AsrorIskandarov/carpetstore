package com.uz.carpetstore.service.auth;

import com.uz.carpetstore.criteria.auth.RoleCriteria;
import com.uz.carpetstore.domain.auth._Role;
import com.uz.carpetstore.dto.auth.RoleCreateDto;
import com.uz.carpetstore.dto.auth.RoleDto;
import com.uz.carpetstore.dto.auth.RoleUpdateDto;
import com.uz.carpetstore.service.IGenericCrudService;

public interface IRoleService extends IGenericCrudService<_Role, RoleDto, RoleCreateDto, RoleUpdateDto, Long, RoleCriteria> {

}