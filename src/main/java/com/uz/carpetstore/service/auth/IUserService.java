package com.uz.carpetstore.service.auth;

import org.springframework.http.ResponseEntity;
import com.uz.carpetstore.criteria.auth.UserCriteria;
import com.uz.carpetstore.domain.auth._User;
import com.uz.carpetstore.dto.auth.AttachRoleDto;
import com.uz.carpetstore.dto.auth.UserCreateDto;
import com.uz.carpetstore.dto.auth.UserDto;
import com.uz.carpetstore.dto.auth.UserUpdateDto;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.IGenericCrudService;

import javax.validation.constraints.NotNull;

public interface IUserService extends IGenericCrudService<_User, UserDto, UserCreateDto, UserUpdateDto, Long, UserCriteria> {

    ResponseEntity<DataDto<UserDto>> attachRolesToUser(@NotNull AttachRoleDto dto);

}
