package com.uz.carpetstore.service.auth;

import org.springframework.http.ResponseEntity;
import com.uz.carpetstore.dto.auth.*;
import com.uz.carpetstore.dto.signUp.EmailConfigDto;
import com.uz.carpetstore.dto.signUp.SendEmailDto;
import com.uz.carpetstore.dto.signUp.UserSignUpDto;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.IAbstractService;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

public interface IAuthService extends IAbstractService {

    ResponseEntity<DataDto<SessionDto>> login(AuthUserDto user, HttpServletRequest request);

    ResponseEntity<DataDto<Boolean>> logout(HttpServletRequest request);

    ResponseEntity<DataDto<SessionDto>> refreshToken(AuthUserRefreshTokenDto user, HttpServletRequest request);

    ResponseEntity<DataDto<Boolean>> changePassword(ChangePasswordDto dto);

    ResponseEntity<DataDto<Boolean>> resetPassword(ResetPasswordDto dto);

    ResponseEntity<DataDto<SessionDto>> signUp(UserSignUpDto dto, HttpServletRequest request);

    void sendEmail(SendEmailDto sendEmailDto) throws MessagingException;

    Boolean enterEmailCode(EmailConfigDto dto);
}
