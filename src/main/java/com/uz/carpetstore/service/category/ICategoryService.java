package com.uz.carpetstore.service.category;

import com.uz.carpetstore.criteria.category.CategoryCriteria;
import com.uz.carpetstore.domain.category._Category;
import com.uz.carpetstore.dto.category.CategoryCreateDto;
import com.uz.carpetstore.dto.category.CategoryDto;
import com.uz.carpetstore.dto.category.CategoryUpdateDto;
import com.uz.carpetstore.service.IGenericCrudService;


public interface ICategoryService extends IGenericCrudService<_Category, CategoryDto, CategoryCreateDto, CategoryUpdateDto, Long, CategoryCriteria> {
}
