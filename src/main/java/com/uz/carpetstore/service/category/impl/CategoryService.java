package com.uz.carpetstore.service.category.impl;

import com.uz.carpetstore.criteria.category.CategoryCriteria;
import com.uz.carpetstore.domain.category._Category;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.category.CategoryCreateDto;
import com.uz.carpetstore.dto.category.CategoryDto;
import com.uz.carpetstore.dto.category.CategoryUpdateDto;
import com.uz.carpetstore.enums.ErrorCodes;
import com.uz.carpetstore.exception.IdRequiredException;
import com.uz.carpetstore.exception.ValidationException;
import com.uz.carpetstore.mapper.GenericMapper;
import com.uz.carpetstore.mapper.category.CategoryMapper;
import com.uz.carpetstore.repository.category.ICategoryRepository;
import com.uz.carpetstore.repository.setting.IErrorRepository;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.GenericCrudService;
import com.uz.carpetstore.service.category.ICategoryService;
import com.uz.carpetstore.service.model.utils.BaseUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Service(value = "categoryService")
public class CategoryService extends GenericCrudService<_Category, CategoryDto, CategoryCreateDto, CategoryUpdateDto, CategoryCriteria, ICategoryRepository> implements ICategoryService {
    /**
     * Common logger for use in subclasses.
     */
    private final Log logger = LogFactory.getLog(getClass());
    private final CategoryMapper categoryMapper;
    private final GenericMapper genericMapper;
    private final Validator validator;


    @Autowired
    public CategoryService(ICategoryRepository repository, BaseUtils utils, IErrorRepository errorRepository, CategoryMapper categoryMapper, GenericMapper genericMapper, Validator validator) {
        super(repository, utils, errorRepository);
        this.categoryMapper = categoryMapper;
        this.genericMapper = genericMapper;
        this.validator = validator;
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_READ)")
    public ResponseEntity<DataDto<CategoryDto>> get(Long id) {
        _Category category = repository.find(CategoryCriteria.childBuilder().selfId(id).build());
        validate(category, id);
        return new ResponseEntity<>(new DataDto<>(categoryMapper.toDto(category)), HttpStatus.OK);
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_READ)")
    public ResponseEntity<DataDto<List<CategoryDto>>> getAll(CategoryCriteria criteria) {
        Long total = repository.getTotalCount(criteria);
        return new ResponseEntity<>(new DataDto<>(categoryMapper.toDto(repository.findAll(criteria)), total), HttpStatus.OK);
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_CREATE)")
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull CategoryCreateDto dto) {
        Set<ConstraintViolation<CategoryCreateDto>> violations = validator.validate(dto);

        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<CategoryCreateDto> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage()).append(" ");
            }
            throw new ValidationException(sb.toString());
        }

        _Category category = categoryMapper.fromCreateDto(dto);
        baseValidation(category);
        category.setName(category.getName());
        repository.save(category);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(category)), HttpStatus.CREATED);
    }

    @Override
    @Transactional
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_UPDATE)")
    public ResponseEntity<DataDto<CategoryDto>> update(@NotNull CategoryUpdateDto dto) {
        if (utils.isEmpty(dto.getId())) {
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Category"));
        }
        _Category category = repository.find(dto.getId());
        validate(category, dto.getId());

        category = categoryMapper.fromUpdateDto(dto, category);
        baseValidation(category);

        category.setName(category.getName());
        category.setUpdatedBy(category.getUpdatedBy());
        category.setUpdatedDate(category.getUpdatedDate());
        repository.save(category);

        return get(category.getId());
    }

    @Override
    @Transactional
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_DELETE)")
    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        _Category category = repository.find(id);
        validate(category, id);
        repository.delete(category);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull _Category entity) {
        if (utils.isEmpty(entity.getName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("name", "Category")));
    }

    @Override
    public void validate(@NotNull _Category entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Category with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Category", id)));
        }
    }
}
