package com.uz.carpetstore.service.model;

import com.uz.carpetstore.criteria.model.ModelCriteria;
import com.uz.carpetstore.domain.model._Model;
import com.uz.carpetstore.dto.model.ModelCreateDto;
import com.uz.carpetstore.dto.model.ModelDto;
import com.uz.carpetstore.dto.model.ModelUpdateDto;
import com.uz.carpetstore.service.IGenericCrudService;

public interface IModelService  extends IGenericCrudService<_Model, ModelDto, ModelCreateDto, ModelUpdateDto, Long, ModelCriteria> {
}
