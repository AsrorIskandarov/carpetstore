package com.uz.carpetstore.service.model.impl;

import com.uz.carpetstore.criteria.model.ModelCriteria;
import com.uz.carpetstore.domain.brand._Brand;
import com.uz.carpetstore.domain.category._Category;
import com.uz.carpetstore.domain.model._Model;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.history.HistoryCreateDto;
import com.uz.carpetstore.dto.model.ModelCreateDto;
import com.uz.carpetstore.dto.model.ModelDto;
import com.uz.carpetstore.dto.model.ModelUpdateDto;
import com.uz.carpetstore.enums.ErrorCodes;
import com.uz.carpetstore.enums.IncomeOutcome;
import com.uz.carpetstore.exception.IdRequiredException;
import com.uz.carpetstore.exception.ValidationException;
import com.uz.carpetstore.mapper.GenericMapper;
import com.uz.carpetstore.mapper.model.MadelMapper;
import com.uz.carpetstore.repository.brand.IBrandRepository;
import com.uz.carpetstore.repository.category.ICategoryRepository;
import com.uz.carpetstore.repository.model.IModelRepository;
import com.uz.carpetstore.repository.setting.IErrorRepository;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.GenericCrudService;
import com.uz.carpetstore.service.hisotory.IHistoryService;
import com.uz.carpetstore.service.model.IModelService;
import com.uz.carpetstore.service.model.utils.BaseUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service(value = "modelService")
public class ModelService extends GenericCrudService<_Model, ModelDto, ModelCreateDto, ModelUpdateDto, ModelCriteria, IModelRepository> implements IModelService {

    /**
     * Common logger for use in subclasses.
     */
    private final Log logger = LogFactory.getLog(getClass());
    private final MadelMapper madelMapper;
    private final GenericMapper genericMapper;
    private final IBrandRepository brandRepository;
    private final ICategoryRepository categoryRepository;
    private final IHistoryService iHistoryService;
    private final Validator validator;


    @Autowired
    public ModelService(IModelRepository repository, BaseUtils utils, IErrorRepository errorRepository, MadelMapper madelMapper, GenericMapper genericMapper, IBrandRepository brandRepository, ICategoryRepository categoryRepository, IHistoryService iHistoryService, Validator validator) {
        super(repository, utils, errorRepository);
        this.madelMapper = madelMapper;
        this.genericMapper = genericMapper;
        this.brandRepository = brandRepository;
        this.categoryRepository = categoryRepository;
        this.iHistoryService = iHistoryService;
        this.validator = validator;
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_READ)")
    public ResponseEntity<DataDto<ModelDto>> get(Long id) {
        _Model model = repository.find(ModelCriteria.childBuilder().selfId(id).build());
        validate(model, id);
        ModelDto modelDto = madelMapper.toDto(model);
        modelDto.setTotalPrice(model.getPrice() * model.getCount());
        return new ResponseEntity<>(new DataDto<>(modelDto), HttpStatus.OK);
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_READ)")
    public ResponseEntity<DataDto<List<ModelDto>>> getAll(ModelCriteria criteria) {
        Long total = repository.getTotalCount(criteria);
        List<Double> totalPrice = Collections.singletonList(0.0);

        List<ModelDto> all = madelMapper.toDto(repository.findAll(criteria));

        for (ModelDto model : all) {
            model.setTotalPrice(model.getPrice() * model.getCount());
        }
        return new ResponseEntity<>(new DataDto<>(all, total), HttpStatus.OK);
    }

    @Override
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_CREATE)")
    public ResponseEntity<DataDto<GenericDto>> create(@NotNull ModelCreateDto dto) {
        Set<ConstraintViolation<ModelCreateDto>> violations = validator.validate(dto);

        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<ModelCreateDto> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage()).append(" ");
            }
            throw new ValidationException(sb.toString());
        }

        _Category category = categoryRepository.find(dto.getCategoryId());
        _Brand brand = brandRepository.find(dto.getBrandId());

        if (brand == null) {
            throw new RuntimeException("This brand not found by this id");
        }
        if (category == null) {
            throw new RuntimeException("This category not found by this id");
        }

        _Model model = madelMapper.fromCreateDto(dto);
        baseValidation(model);
        model.setName(model.getName());
        model.setCount(model.getCount());
        model.setCreatedDate(model.getCreatedDate());
        model.setBrand(brand);
        model.setCategory(category);
        repository.save(model);

        iHistoryService.create(HistoryCreateDto.builder().modelId(model.getId()).incomeOutcome(IncomeOutcome.INCOME).price(dto.getPrice()).count(dto.getCount()).build());
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(model)), HttpStatus.CREATED);
    }

    @Override
    @Transactional
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_UPDATE)")
    public ResponseEntity<DataDto<ModelDto>> update(@NotNull ModelUpdateDto dto) {

        if (utils.isEmpty(dto.getId())) {
            throw new IdRequiredException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_ID_REQUIRED, "Model"));
        }
        _Model model = repository.find(dto.getId());
        validate(model, dto.getId());


        if (dto.getIncomeOutcome().equals(IncomeOutcome.INCOME)) {
            dto.setCount(model.getCount() + dto.getCount());
            iHistoryService.create(HistoryCreateDto.builder().modelId(model.getId()).incomeOutcome(IncomeOutcome.INCOME).price(dto.getPrice()).count(dto.getCount()).build());
        } else {
            dto.setCount(model.getCount() - dto.getCount());
            iHistoryService.create(HistoryCreateDto.builder().modelId(model.getId()).incomeOutcome(IncomeOutcome.OUTCOME).price(dto.getPrice()).count(dto.getCount()).build());
        }

        model = madelMapper.fromUpdateDto(dto, model);
        baseValidation(model);
        repository.save(model);
        return get(model.getId());
    }

    @Override
    @Transactional
//    @PreAuthorize("hasPermission(null, T(uz.personal.enums.Permissions).PLAYER_DELETE)")
    public ResponseEntity<DataDto<Boolean>> delete(@NotNull Long id) {
        _Model model = repository.find(id);
        validate(model, id);
        repository.delete(model);

        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);
    }

    @Override
    public void baseValidation(@NotNull _Model entity) {
        if (utils.isEmpty(entity.getName()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("name", "Model")));
        if (utils.isEmpty(entity.getCount()))
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_GIVEN_FIELD_REQUIRED, utils.toErrorParams("count", "Model")));
    }

    @Override
    public void validate(@NotNull _Model entity, @NotNull Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("Model with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("Model", id)));
        }
    }
}
