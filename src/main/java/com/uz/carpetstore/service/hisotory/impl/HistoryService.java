package com.uz.carpetstore.service.hisotory.impl;

import com.uz.carpetstore.criteria.history.HistoryCriteria;
import com.uz.carpetstore.domain.history._History;
import com.uz.carpetstore.domain.model._Model;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.history.HistoryCreateDto;
import com.uz.carpetstore.dto.history.HistoryDto;
import com.uz.carpetstore.dto.history.HistoryUpdateDto;
import com.uz.carpetstore.enums.ErrorCodes;
import com.uz.carpetstore.exception.ValidationException;
import com.uz.carpetstore.mapper.GenericMapper;
import com.uz.carpetstore.mapper.history.HistoryMapper;
import com.uz.carpetstore.repository.history.IHistoryRepository;
import com.uz.carpetstore.repository.model.IModelRepository;
import com.uz.carpetstore.repository.setting.IErrorRepository;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.GenericCrudService;
import com.uz.carpetstore.service.hisotory.IHistoryService;
import com.uz.carpetstore.service.model.utils.BaseUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

@Service
public class HistoryService extends GenericCrudService<_History, HistoryDto, HistoryCreateDto, HistoryUpdateDto, HistoryCriteria, IHistoryRepository> implements IHistoryService {

    private final Log logger = LogFactory.getLog(getClass());
    private final HistoryMapper historyMapper;
    private final GenericMapper genericMapper;
    private final IModelRepository modelRepository;
    private final Validator validator;


    public HistoryService(IHistoryRepository repository, BaseUtils utils, IErrorRepository errorRepository, HistoryMapper historyMapper, GenericMapper genericMapper, IModelRepository modelRepository, Validator validator) {
        super(repository, utils, errorRepository);
        this.historyMapper = historyMapper;
        this.genericMapper = genericMapper;
        this.modelRepository = modelRepository;
        this.validator = validator;
    }

    @Override
    public ResponseEntity<DataDto<HistoryDto>> get(Long id) {
        _History history = repository.find(HistoryCriteria.childBuilder().selfId(id).build());
        validate(history, id);
        return new ResponseEntity<>(new DataDto<>(historyMapper.toDto(history)), HttpStatus.OK);

    }

    @Override
    public ResponseEntity<DataDto<List<HistoryDto>>> getAll(HistoryCriteria criteria) {
        Long total = repository.getTotalCount(criteria);
        return new ResponseEntity<>(new DataDto<>(historyMapper.toDto(repository.findAll(criteria)), total), HttpStatus.OK);

    }

    @Override
    public ResponseEntity<DataDto<GenericDto>> create(HistoryCreateDto dto) {
        Set<ConstraintViolation<HistoryCreateDto>> violations = validator.validate(dto);

        if (!violations.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (ConstraintViolation<HistoryCreateDto> constraintViolation : violations) {
                sb.append(constraintViolation.getMessage()).append(" ");
            }
            throw new ValidationException(sb.toString());
        }

        _Model model = modelRepository.find(dto.getModelId());

        _History history = new _History();
        baseValidation(history);
        history.setStatus(dto.getIncomeOutcome());
        history.setPrice(dto.getPrice());
        history.setCount(dto.getCount());
        history.setModel(model);
        repository.save(history);
        return new ResponseEntity<>(new DataDto<>(genericMapper.fromDomain(history)), HttpStatus.CREATED);

    }

    @Override
    public ResponseEntity<DataDto<HistoryDto>> update(HistoryUpdateDto dto) {

        return null;
    }

    @Override
    public ResponseEntity<DataDto<Boolean>> delete(Long id) {

        _History history = repository.find(id);
        validate(history, id);
        repository.delete(history);
        return new ResponseEntity<>(new DataDto<>(true), HttpStatus.OK);

    }

    @Override
    public void baseValidation(_History entity) {
        super.baseValidation(entity);
    }

    @Override
    public void validate(_History entity, Long id) {
        if (utils.isEmpty(entity)) {
            logger.error(String.format("History with id '%s' not found", id));
            throw new ValidationException(errorRepository.getErrorMessage(ErrorCodes.OBJECT_NOT_FOUND_ID, utils.toErrorParams("History", id)));

        }
    }
}
