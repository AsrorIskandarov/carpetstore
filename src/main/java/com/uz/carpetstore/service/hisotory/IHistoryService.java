package com.uz.carpetstore.service.hisotory;

import com.uz.carpetstore.criteria.history.HistoryCriteria;
import com.uz.carpetstore.domain.history._History;
import com.uz.carpetstore.dto.history.HistoryCreateDto;
import com.uz.carpetstore.dto.history.HistoryDto;
import com.uz.carpetstore.dto.history.HistoryUpdateDto;
import com.uz.carpetstore.service.IGenericCrudService;

public interface IHistoryService extends IGenericCrudService<_History, HistoryDto, HistoryCreateDto, HistoryUpdateDto, Long, HistoryCriteria> {
}
