package com.uz.carpetstore.service;

import com.uz.carpetstore.domain.auth._User;
import com.uz.carpetstore.dto.auth.CustomUserDetails;
import com.uz.carpetstore.mapper.auth.UserMapper;
import com.uz.carpetstore.repository.auth.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final IUserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public UserDetailsService(IUserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        _User user = userRepository.findByUsername(username);
        if (user == null)
            throw new RuntimeException(String.format("User with username '%s' not found", username));
        if (user.isLocked())
            throw new RuntimeException(String.format("User with username '%s' is locked", username));
        if (!user.isEnabled())
            throw new RuntimeException(String.format("User with username '%s' is disabled", username));

        return new CustomUserDetails(user, userMapper.toDto(user));
    }
}
