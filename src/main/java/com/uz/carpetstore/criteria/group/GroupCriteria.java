package com.uz.carpetstore.criteria.group;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import com.uz.carpetstore.criteria.GenericCriteria;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = PRIVATE)
public class GroupCriteria  extends GenericCriteria {
    private boolean isActivated;

    private String username;
}
