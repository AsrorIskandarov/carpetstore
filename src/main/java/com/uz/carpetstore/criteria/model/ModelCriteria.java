package com.uz.carpetstore.criteria.model;

import lombok.*;
import com.uz.carpetstore.criteria.GenericCriteria;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ModelCriteria extends GenericCriteria {
    private String name;

    private Integer count;

    private Double price;

    @Builder(builderMethodName = "childBuilder")
    public ModelCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String name, Integer count, Double price) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.name = name;
        this.count = count;
        this.price=price;
    }
}
