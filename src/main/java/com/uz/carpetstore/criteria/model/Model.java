package com.uz.carpetstore.criteria.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.uz.carpetstore.criteria.GenericCriteria;
@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
public class Model extends GenericCriteria {
}
