package com.uz.carpetstore.criteria.brand;

import lombok.*;
import com.uz.carpetstore.criteria.GenericCriteria;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BrandCriteria extends GenericCriteria {
    private String name;

    @Builder(builderMethodName = "childBuilder")
    public BrandCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, String name) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.name = name;
    }
}
