package com.uz.carpetstore.criteria.history;

import lombok.*;
import lombok.experimental.FieldDefaults;
import com.uz.carpetstore.criteria.GenericCriteria;
import com.uz.carpetstore.enums.IncomeOutcome;

import static lombok.AccessLevel.PRIVATE;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = PRIVATE)
public class HistoryCriteria extends GenericCriteria {

    IncomeOutcome status;

    Integer count;

    Double price;

    @Builder(builderMethodName = "childBuilder")
    public HistoryCriteria(Long selfId, Integer page, Integer perPage, String sortBy, String sortDirection, IncomeOutcome status, Integer count, Double price) {
        super(selfId, page, perPage, sortBy, sortDirection);
        this.status = status;
        this.count = count;
        this.price=price;
    }

}
