package com.uz.carpetstore.controller.brand;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.uz.carpetstore.controller.ApiController;
import com.uz.carpetstore.criteria.brand.BrandCriteria;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.brand.BrandCreateDto;
import com.uz.carpetstore.dto.brand.BrandDto;
import com.uz.carpetstore.dto.brand.BrandUpdateDto;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.brand.IBrandService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Brand controller", tags = "brand-controller")
@RestController
public class BrandController  extends ApiController<IBrandService> {
    public BrandController(IBrandService service) {super(service);}
    @ApiOperation(value = "Get Single Brand ")
    @RequestMapping(value = API_PATH + V_1 + "/brand/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<BrandDto>> getBrand(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Brand")
    @RequestMapping(value = API_PATH + V_1 + "/brand/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<BrandDto>>> getAllBrands(@Valid BrandCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Brand Create")
    @RequestMapping(value = API_PATH + V_1 + "/brand/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createBrand(@RequestBody BrandCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Brand Update")
    @RequestMapping(value = API_PATH + V_1 + "/brand/update", method = RequestMethod.PUT)
    public ResponseEntity<DataDto<BrandDto>> updateBrand(@RequestBody BrandUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Brand Delete")
    @RequestMapping(value = API_PATH + V_1 + "/brand/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DataDto<Boolean>> deleteBrand(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}
