package com.uz.carpetstore.controller.category;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.uz.carpetstore.controller.ApiController;
import com.uz.carpetstore.criteria.category.CategoryCriteria;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.category.CategoryCreateDto;
import com.uz.carpetstore.dto.category.CategoryDto;
import com.uz.carpetstore.dto.category.CategoryUpdateDto;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.category.ICategoryService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Category controller", tags = "category-controller")
@RestController
public class CategoryController extends ApiController<ICategoryService> {
    public CategoryController(ICategoryService service) {super(service);}
    @ApiOperation(value = "Get Single Category ")
    @RequestMapping(value = API_PATH + V_1 + "/category/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<CategoryDto>> getCategory(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Category")
    @RequestMapping(value = API_PATH + V_1 + "/category/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<CategoryDto>>> getAllCategories(@Valid CategoryCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Category Create")
    @RequestMapping(value = API_PATH + V_1 + "/category/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createCategory(@RequestBody CategoryCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Category Update")
    @RequestMapping(value = API_PATH + V_1 + "/category/update", method = RequestMethod.PUT)
    public ResponseEntity<DataDto<CategoryDto>> updateCategory(@RequestBody CategoryUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Category Delete")
    @RequestMapping(value = API_PATH + V_1 + "/category/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DataDto<Boolean>> deleteCategory(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}
