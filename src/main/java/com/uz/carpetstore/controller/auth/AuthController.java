package com.uz.carpetstore.controller.auth;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.uz.carpetstore.controller.ApiController;
import com.uz.carpetstore.dto.auth.*;
import com.uz.carpetstore.dto.signUp.EmailConfigDto;
import com.uz.carpetstore.dto.signUp.SendEmailDto;
import com.uz.carpetstore.dto.signUp.UserSignUpDto;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.auth.IAuthService;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Api(value = "Auth controller")
@RestController
public class AuthController extends ApiController<IAuthService> {

    public AuthController(IAuthService service) {
        super(service);
    }

    @RequestMapping(value = LOGIN_URL, method = RequestMethod.POST)
    public ResponseEntity<DataDto<SessionDto>> login(@RequestBody AuthUserDto dto, HttpServletRequest request) {
        return service.login(dto, request);
    }

    @RequestMapping(value = SIGN_UP, method = RequestMethod.POST)
    public ResponseEntity<DataDto<SessionDto>> signUp(@RequestBody UserSignUpDto dto, HttpServletRequest request) {
        return service.signUp(dto, request);
    }

    @RequestMapping(value = SEND_EMAIL, method = RequestMethod.POST)
    public void sendEmail() throws MessagingException {
        Integer code= new Random().nextInt(900000)+100000;

        Map<String, Object> model = new HashMap<>();
        model.put("name", "Asror");
        model.put("code", code);
        SendEmailDto sendEmailDto = new SendEmailDto();
        sendEmailDto.setSendTo("iskandarovasror998@gmail.com");
        sendEmailDto.setSubjectName("Salom!");
        sendEmailDto.setTemplateName("mail-template.ftl");
        sendEmailDto.setModel(model);
        service.sendEmail(sendEmailDto);
    }

    @RequestMapping(value = ENTER_CODE, method = RequestMethod.GET)
    public Boolean checkEmailCode(EmailConfigDto dto) {
        return service.enterEmailCode(dto);
    }

    @RequestMapping(value = LOGOUT_URL, method = RequestMethod.GET)
    public ResponseEntity<DataDto<Boolean>> logout(HttpServletRequest request) {
        return service.logout(request);
    }

    @RequestMapping(value = REFRESH_TOKEN_URL, method = RequestMethod.POST)
    public ResponseEntity<DataDto<SessionDto>> refreshToken(@RequestBody AuthUserRefreshTokenDto dto, HttpServletRequest request) {
        return service.refreshToken(dto, request);
    }

    @ApiOperation(value = "Change User Password")
    @RequestMapping(value = AUTH + "/change/password", method = RequestMethod.POST)
    public ResponseEntity<DataDto<Boolean>> changePassword(@RequestBody ChangePasswordDto dto) {
        return service.changePassword(dto);
    }

    @ApiOperation(value = "Reset User Password")
    @RequestMapping(value = AUTH + "/reset/password", method = RequestMethod.POST)
    public ResponseEntity<DataDto<Boolean>> resetPassword(@RequestBody ResetPasswordDto dto) {
        return service.resetPassword(dto);
    }
}
