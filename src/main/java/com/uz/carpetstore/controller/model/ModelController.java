package com.uz.carpetstore.controller.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.uz.carpetstore.controller.ApiController;
import com.uz.carpetstore.criteria.model.ModelCriteria;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.model.ModelCreateDto;
import com.uz.carpetstore.dto.model.ModelDto;
import com.uz.carpetstore.dto.model.ModelUpdateDto;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.model.IModelService;

import javax.validation.Valid;
import java.util.List;

@Api(value = "Model controller", tags = "model-controller")
@RestController
public class ModelController extends ApiController<IModelService> {
    public ModelController(IModelService service) {super(service);}

    @ApiOperation(value = "Get Single Model ")
    @RequestMapping(value = API_PATH + V_1 + "/model/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<ModelDto>> getModel(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List Model")
    @RequestMapping(value = API_PATH + V_1 + "/model/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<ModelDto>>> getAllModels(@Valid ModelCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "Model Create")
    @RequestMapping(value = API_PATH + V_1 + "/model/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createModel(@RequestBody ModelCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "Model Update")
    @RequestMapping(value = API_PATH + V_1 + "/model/update", method = RequestMethod.PUT)
    public ResponseEntity<DataDto<ModelDto>> updateModel(@RequestBody ModelUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "Model Delete")
    @RequestMapping(value = API_PATH + V_1 + "/model/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DataDto<Boolean>> deleteModel(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }
}
