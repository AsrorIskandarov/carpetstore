package com.uz.carpetstore.controller.history;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.uz.carpetstore.controller.ApiController;
import com.uz.carpetstore.criteria.history.HistoryCriteria;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.history.HistoryCreateDto;
import com.uz.carpetstore.dto.history.HistoryDto;
import com.uz.carpetstore.dto.history.HistoryUpdateDto;
import com.uz.carpetstore.response.DataDto;
import com.uz.carpetstore.service.hisotory.IHistoryService;

import javax.validation.Valid;
import java.util.List;

@RestController
public class HistoryController extends ApiController<IHistoryService> {
    public HistoryController(IHistoryService service) {
        super(service);
    }

    @ApiOperation(value = "Get Single History ")
    @RequestMapping(value = API_PATH + V_1 + "/history/get/{id}", method = RequestMethod.GET)
    public ResponseEntity<DataDto<HistoryDto>> getModel(@PathVariable(value = "id") Long id) {
        return service.get(id);
    }

    @ApiOperation(value = "Get List history")
    @RequestMapping(value = API_PATH + V_1 + "/history/get/all", method = RequestMethod.GET)
    public ResponseEntity<DataDto<List<HistoryDto>>> getAllModels(@Valid HistoryCriteria criteria) {
        return service.getAll(criteria);
    }

    @ApiOperation(value = "history Create")
    @RequestMapping(value = API_PATH + V_1 + "/history/create", method = RequestMethod.POST)
    public ResponseEntity<DataDto<GenericDto>> createModel(@RequestBody HistoryCreateDto dto) {
        return service.create(dto);
    }

    @ApiOperation(value = "history Update")
    @RequestMapping(value = API_PATH + V_1 + "/history/update", method = RequestMethod.PUT)
    public ResponseEntity<DataDto<HistoryDto>> updateModel(@RequestBody HistoryUpdateDto dto) {
        return service.update(dto);
    }

    @ApiOperation(value = "history Delete")
    @RequestMapping(value = API_PATH + V_1 + "/history/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<DataDto<Boolean>> deleteModel(@PathVariable(value = "id") Long id) {
        return service.delete(id);
    }









}

