package com.uz.carpetstore.dto.history;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;
import com.uz.carpetstore.dto.GenericCrudDto;
import com.uz.carpetstore.enums.IncomeOutcome;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@FieldDefaults(level = PRIVATE)
@Component
public class HistoryCreateDto extends GenericCrudDto {

    Long modelId;

    IncomeOutcome incomeOutcome;

    Integer count;

    Double price;

}
