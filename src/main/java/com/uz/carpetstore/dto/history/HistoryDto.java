package com.uz.carpetstore.dto.history;

import lombok.*;
import lombok.experimental.FieldDefaults;
import com.uz.carpetstore.domain.model._Model;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.enums.IncomeOutcome;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
@AllArgsConstructor
public class HistoryDto extends GenericDto {

    IncomeOutcome status;

    Integer count;

    Double price;

    _Model model;

    @Builder(builderMethodName = "childBuilder")
    public HistoryDto(Long id, IncomeOutcome status, Integer count, Double price, _Model model) {
        super(id);
        this.status = status;
        this.count = count;
        this.price = price;
        this.model = model;
    }

}



















