package com.uz.carpetstore.dto.history;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import com.uz.carpetstore.dto.GenericCrudDto;
import com.uz.carpetstore.enums.IncomeOutcome;

import static lombok.AccessLevel.PRIVATE;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = PRIVATE)
public class HistoryUpdateDto extends GenericCrudDto {

    Long id;

    IncomeOutcome status;

    Integer count;

    Double price;

    Long modelId;

}


