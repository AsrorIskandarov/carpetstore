package com.uz.carpetstore.dto.brand;

import lombok.*;
import com.uz.carpetstore.dto.GenericDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BrandDto extends GenericDto {
    private String name;

    @Builder(builderMethodName = "childBuilder")
    public BrandDto(Long id, String name) {
        super(id);
        this.name = name;
    }
}
