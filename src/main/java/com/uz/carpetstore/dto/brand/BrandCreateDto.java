package com.uz.carpetstore.dto.brand;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.uz.carpetstore.dto.GenericCrudDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Brand create request")
public class BrandCreateDto   extends GenericCrudDto {

    @ApiModelProperty(value = "name", required = true)
    private String name;

}
