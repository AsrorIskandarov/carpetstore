package com.uz.carpetstore.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TranslationDto {

    private String name;

    private String nameRu;

    private String nameEn;

    private String nameUzl;
}
