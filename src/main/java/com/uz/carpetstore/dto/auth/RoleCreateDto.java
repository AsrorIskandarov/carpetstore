package com.uz.carpetstore.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.uz.carpetstore.dto.GenericCrudDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Role create request")
public class RoleCreateDto extends GenericCrudDto {

    @ApiModelProperty(value = "CODE", required = true)
    private String code;

    @ApiModelProperty(required = true)
    private String name;
}
