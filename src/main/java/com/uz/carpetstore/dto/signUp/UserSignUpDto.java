package com.uz.carpetstore.dto.signUp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserSignUpDto {
    @NotNull
    private  String firstName;
    @NotNull
    private String lastName;

    @Column(unique = true)
    private String phoneNumber;

    @NotNull
    @Column(unique = true)
    private String email;

    @NotNull
    @Column(unique = true)
    private String username;
    @NotNull
    private String password;

}
