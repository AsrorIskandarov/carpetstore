package com.uz.carpetstore.dto.signUp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SendEmailDto {
    private String sendTo;
    private String templateName;
    private String subjectName;
    private Map<String, Object> model;
}
