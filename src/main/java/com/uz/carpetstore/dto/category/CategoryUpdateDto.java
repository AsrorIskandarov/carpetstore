package com.uz.carpetstore.dto.category;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.uz.carpetstore.dto.CrudDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Category update request")
public class CategoryUpdateDto implements CrudDto {
    @ApiModelProperty(required = true)
    private Long id;

    @ApiModelProperty
    private String name;

}
