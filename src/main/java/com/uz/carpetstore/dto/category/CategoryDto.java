package com.uz.carpetstore.dto.category;

import lombok.*;
import com.uz.carpetstore.dto.GenericDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDto extends GenericDto {
    private String name;

    @Builder(builderMethodName = "childBuilder")
    public CategoryDto(Long id, String name) {
        super(id);
        this.name = name;
    }
}
