package com.uz.carpetstore.dto.group;

import com.uz.carpetstore.dto.CrudDto;

import javax.persistence.Column;
import javax.persistence.Id;

public class GroupUpdateDto implements CrudDto {
    //    chatId
    @Id
    @Column(name = "id", nullable = false)
    private Long id;


    private boolean isActivated;

    private String username;
}
