package com.uz.carpetstore.dto.group;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.uz.carpetstore.dto.CrudDto;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GroupCreateDto implements CrudDto {

    //    chatId
    private Long id;

    private String username;
}
