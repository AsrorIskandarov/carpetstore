package com.uz.carpetstore.dto.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.uz.carpetstore.dto.GenericCrudDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Model create request")
public class ModelCreateDto  extends GenericCrudDto {

    @ApiModelProperty(value = "name", required = true)
    private String name;

    @ApiModelProperty(required = true)
    private Integer count;

    @ApiModelProperty(required = true)
    private Double price;

    @ApiModelProperty(required = true)
    private Long brandId;

    @ApiModelProperty(required = true)
    private Long categoryId;
}
