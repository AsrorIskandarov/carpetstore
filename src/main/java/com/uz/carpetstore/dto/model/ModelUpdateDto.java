package com.uz.carpetstore.dto.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.uz.carpetstore.dto.GenericCrudDto;
import com.uz.carpetstore.enums.IncomeOutcome;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Model update request")
public class ModelUpdateDto  extends GenericCrudDto {
    @ApiModelProperty(required = true)
    private Long id;

    private String name;

    private Integer count;

    private Double price;

    IncomeOutcome incomeOutcome;

//    private Long brandId;

//    private Long categoryId;
}
