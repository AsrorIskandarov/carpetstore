package com.uz.carpetstore.dto.model;

import lombok.*;
import com.uz.carpetstore.dto.GenericDto;
import com.uz.carpetstore.dto.brand.BrandDto;
import com.uz.carpetstore.dto.category.CategoryDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ModelDto extends GenericDto {
    private String name;

    private Integer count;

    private Double price;

    private Double totalPrice;

    private BrandDto brand;

    private CategoryDto category;

    @Builder(builderMethodName = "childBuilder")
    public ModelDto(Long id, String name, Integer count, Double price, Double totalPrice, BrandDto brand, CategoryDto category) {
        super(id);
        this.name = name;
        this.count = count;
        this.price = price;
        this.totalPrice = totalPrice;
        this.brand = brand;
        this.category = category;
    }
}
