package com.uz.carpetstore.enums;

public enum State {
    NEW,
    UPDATED,
    DELETED,
    IN_PROGRESS
}
